import React, { Component } from 'react';

import Message from './message';

class MessageList extends Component {
  render() {
    const messages = this.props.messages.map((message) => <Message key={message.id} isOwnMessage={message.botMessage}>{message.text}</Message>);
    return (
      <div className="messages is-marginless" hidden={this.props.hidden}>
        <ul>
          {messages}
        </ul>
      </div>
    );
  }
}

export default MessageList;
