import React, { Component } from 'react';
import HistoryPanel from './history-panel';
import DialogPanel from './dialog-panel';

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedChatId: null
    };

    this.handleClick = this.handleClick.bind(this);
    this.updateLastMessages = this.updateLastMessages.bind(this);
  }

  render() {
    return (
      <section className="section is-paddingless">
        <div className="columns is-gapless">
          <DialogPanel messages={this.props.messages} onClick={this.handleClick}/>
          <HistoryPanel chatId={this.state.selectedChatId} messages={this.props.messages} onSubmit={this.updateLastMessages} />
        </div>
      </section>
    );
  }

  handleClick(clickEvent) {
    const chatId = Number(clickEvent.currentTarget.dataset.chatId);
    this.setState({
      selectedChatId: chatId
    });
  }

  updateLastMessages(lastMessage) {
    const randomId = Math.floor(Math.random() * (100000 - 150)) + 150;
    const newMessage = {
      botMessage: true,
      text: lastMessage,
      time: new Date().getTime(),
      id: randomId
    };
    this.props.onUpdate(this.state.selectedChatId, newMessage);
  }
}

export default Container;
