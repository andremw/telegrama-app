import React, { Component } from 'react';

import MessageList from './messages-list';
import Textfield from './textfield';

class HistoryPanel extends Component {
  render() {
    const { messages, chatId } = this.props;
    const chatMessages = messages.find((message) => message.chatId === chatId) || {};
    const lastMessages = chatMessages.lastMessages || [];
    return (
      <div className="history column">
        <MessageList messages={lastMessages} hidden={lastMessages.length <= 0} />
        <Textfield onSubmit={this.props.onSubmit} hidden={lastMessages.length <= 0} />
      </div>
    );
  }
}

export default HistoryPanel;
