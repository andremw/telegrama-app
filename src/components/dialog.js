import React, { Component } from 'react';

class Dialog extends Component {
  render() {
    const defaultPhoto = 'https://placeholdit.imgix.net/~text?txtsize=6&txt=48%C3%9748&w=48&h=48';
    const { chatId, lastMessages, from: { name } } = this.props.data;
    const lastMessage = lastMessages[lastMessages.length - 1];
    const userProfilePhoto = lastMessage.userProfilePhoto || defaultPhoto;
    const isBotMessage = lastMessage.botMessage;
    const text = `${isBotMessage ? 'You: ' : ''}${lastMessage.text}`;
    return (
      <li onClick={this.props.onClick} data-chat-id={chatId}>
        <article className="media">
          <figure className="user-pic media-left">
            <p className="image is-48x48">
              <img src={userProfilePhoto} alt=""></img>
            </p>
          </figure>
          <div className="media-content">
            <div className="content">
              <p>
                <strong>{name}</strong>
                <br/>
                {text}
              </p>
            </div>
          </div>
        </article>
      </li>
    );
  }
}

export default Dialog;
