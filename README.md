# Telegrama-App
----
#### Telegrama-App is the graphical interface for connecting the Admin Bot to Telegram's BOT API.

### Quick Start
1. Clone the repo and go to the root of the project
2. Run `yarn`

### Starting the React Server
Run `yarn start`. "Create-react-app" will proxy the requests to the address configured as "proxy" 
in package.json (currently 3001).

### Unit tests
Run `yarn test`

### Building the project
Run `yarn build`

Telegrama-App was created with create-react-app, and uses Socket.io for receiving real-time updates.
