import React, { Component } from 'react';

class Message extends Component {
  render() {
    return (
      <li className={this.getMessageClass(this.props.isOwnMessage)}>
        {this.props.children}
      </li>
    );
  }

  getMessageClass(isOwnMessage) {
    const basicClass = 'message clear';
    if (isOwnMessage) {
      return `${basicClass} is-pulled-right`;
    }
    return `${basicClass} from-them is-pulled-left`;
  }
}

export default Message;
