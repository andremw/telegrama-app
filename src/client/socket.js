import socket from 'socket.io-client';

function startSocket(callback) {
  const io = socket('http://localhost:3001');

  io.on('new:user-message', callback);
}

export default startSocket;
