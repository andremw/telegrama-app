import React, { Component } from 'react';
import Logout from './logout';

class Nav extends Component {
  render() {
    return (
      <nav className="nav has-shadow">
        <div className="nav-left">
          <a className="nav-item">
            <img src="https://cdn1.iconfinder.com/data/icons/mixed-bag/32/robot-01-128.png" alt="RosieeeBot" />
          </a>
          <span className="nav-item">RosieeeBot</span>
        </div>
        <div className="nav-center">
          <Logout />
        </div>
      </nav>
    );
  }
}

export default Nav;
