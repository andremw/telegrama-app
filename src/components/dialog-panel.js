import React, { Component } from 'react';
import Dialog from './dialog';

class DialogPanel extends Component {
  render() {
    const dialogs = this.props.messages.map((message) => {
      const data = {
        from: message.from,
        lastMessages: message.lastMessages,
        chatId: message.chatId,
        userProfilePhoto: message.userProfilePhoto,
      };

      return <Dialog onClick={this.props.onClick} key={message.chatId} data={data} />
    });
    return (
      <div className="dialogs column is-4">
        <ul>
          {dialogs}
        </ul>
      </div>
    );
  }
}

export default DialogPanel;
