import React, { Component } from 'react';

class Textfield extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  render() {
    return (
      <div className="text-area" hidden={this.props.hidden}>
        <div className="field is-grouped">
          <p className="control is-expanded">
            <input type="text" className="input" ref="input" placeholder="Send a message" onChange={this.handleChange} value={this.state.value}/>
          </p>
          <p className="control">
            <a onClick={this.handleClick} className="button is-info">
              SEND
            </a>
          </p>
        </div>
      </div>
    );
  }

  handleChange(changeEvent) {
    this.setState({
      value: changeEvent.target.value
    })
  }

  handleClick() {
    this.props.onSubmit(this.state.value);
    this.refs.input.value = '';
    this.setState({
      value: ''
    });
  }
}

export default Textfield;
