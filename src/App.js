import client from './client';
import startSocket from './client/socket';

import React, { Component } from 'react';
import './App.css';

import Nav from './components/nav';
import Container from './components/container';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dialogs: []
    };

    client.getMessages().then((messages) => {
      this.setState({
        dialogs: messages
      });
    });

    this.updateMessagesForChatId = this.updateMessagesForChatId.bind(this);
    this.handleSocketData = this.handleSocketData.bind(this);
    startSocket(this.handleSocketData);
  }

  render() {
    return (
      <div className="App container is-fluid is-marginless">
        <div className="columns">
          <div className="column is-2"></div>

          <div className="column">
            <Nav />
            <Container messages={this.state.dialogs} onUpdate={this.updateMessagesForChatId}/>
          </div>

          <div className="column is-2"></div>
        </div>
      </div>
    );
  }

  updateMessagesForChatId(chatId, newMessage) {
    client.postMessage({
      chat_id: chatId,
      text: newMessage.text
    }).then(() => {
      this.updateNewMessages(chatId, newMessage);
    });
  }

  updateNewMessages(chatId, newMessage) {
    const dialogs = this.state.dialogs.slice(0);
    let existingDialog = dialogs.find((dialog) => dialog.chatId === chatId);
    if (!existingDialog) {
      const dialog = {
        chatId,
        from: {
          id: newMessage.userId,
          name: newMessage.name,
          username: newMessage.username
        },
        date: newMessage.time,
        lastMessages: [{
          id: newMessage.id,
          botMessage: false,
          text: newMessage.text,
          time: newMessage.time,
          userProfilePhoto: newMessage.userProfilePhoto
        }]
      }
      dialogs.push(dialog);
    } else {
      existingDialog.lastMessages.push(newMessage);
    }

    this.setState({
      dialogs
    });
  }

  handleSocketData(data) {
    this.updateNewMessages(data.chatId, data);
  }
}

export default App;
