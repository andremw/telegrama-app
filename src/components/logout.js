import React, { Component } from 'react';

class Logout extends Component {
  render() {
    return (
      <a className="nav-item">
        Logout
      </a>
    );
  }
}

export default Logout
