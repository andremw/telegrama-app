const client = {
  getMessages() {
    return fetch('/messages', {
      accept: 'application/json'
    }).then((response) => {
      return response.json();
    });
  },
  postMessage(message) {
    return fetch('/messages', {
      method: 'POST',
      body: JSON.stringify(message),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      return response.json();
    });
  }
};

export default client;
